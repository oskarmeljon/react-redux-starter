## React & Redux starter
---
### This starter provides ability to quick start (almost immediately) working on project. You won't find here any complicated configurations or fancy scripts doing something, because I think it's not what I would like to this package be about. I assume that you'll be able to implement that on your own if needed.

---

## Inside starter:
* React (16.9)
* Redux (4.0)
* Redux thunk (2.3)
* Styled components (4.3)
* React router DOM (5.0)
* Redux logger (3.0)

---
## Instalation
* Clone repository
    ```
    git clone https://github.com/oskarmeljon/react-redux-starter.git
    ```
    or using SSH
    ```
    git clone git@github.com:oskarmeljon/react-redux-starter.git
    ```

* Install dependencies
    ```
    cd react-redux-starter
    ```
    ```
    npm install
    ```

## Run
### In order to run app, type in terminal
```
    npm start
```
