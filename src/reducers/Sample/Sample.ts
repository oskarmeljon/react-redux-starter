import { TYPES } from 'action-types/Sample';

// define shape of defaultState & state
export interface IState {}

// define shape of actions
interface IAction {
    type: string;
}

const defaultState: IState = {};

export const reducer = (state: IState = defaultState, action: IAction): IState => {
    switch (action.type) {
        case TYPES.SAMPLE_ACTION.PENDING:
            return {
                ...state,
                // set loading state, clear errors etc.
            };
        case TYPES.SAMPLE_ACTION.FINISHED:
            return {
                ...state,
                // do something based on data from action
            };
        case TYPES.SAMPLE_ACTION.REJECTED:
            return {
                ...state,
            }
        default:
            return state;
    }
};