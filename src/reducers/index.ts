import { combineReducers } from 'redux';
import { reducer as defaultReducer, IState } from 'reducers/Sample/Sample';

export interface IStore {
    readonly defaultReducer: IState;
}
export default combineReducers({
    defaultReducer
});