import React, { FunctionComponent } from 'react';

export const HelloWorld: FunctionComponent = () => <p>Hello world</p>;