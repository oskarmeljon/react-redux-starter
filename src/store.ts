import { applyMiddleware, createStore, Middleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import reducer from 'reducers/index';

const createDevelopmentMiddleware = () => {
    const logger: Middleware = createLogger();
    return applyMiddleware(thunk, logger);
};

const createProductionMiddleware = () => applyMiddleware(thunk);

const middleware = process.env.NODE_ENV !== 'production' ? createDevelopmentMiddleware() : createProductionMiddleware();
const store = createStore(reducer, middleware);

export default store;
