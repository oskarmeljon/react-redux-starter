import { createActionsConstants } from 'utilities/actions';

export const TYPES = {
    SAMPLE_ACTION: createActionsConstants('SAMPLE_ACTION')
};