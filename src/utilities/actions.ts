export const createActionsConstants = (actionName: string) => ({
    PENDING: `${actionName}_PENDING`,
    FINISHED: `${actionName}_FINISHED`,
    REJECTED: `${actionName}_REJECTED`,
});