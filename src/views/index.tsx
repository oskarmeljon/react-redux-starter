import React from 'react';
import { Route, Redirect, Switch, withRouter, RouteComponentProps } from 'react-router-dom';
import { HelloWorld } from 'components/HelloWorld/HelloWorld';

interface IRoutingConfiguration extends RouteComponentProps<{}> {};

const _RoutingConfiguration: React.FunctionComponent<IRoutingConfiguration> = () => (
    <Switch>
        <Route path="/" component={HelloWorld} />
        <Redirect to="/" />
    </Switch>
);

export const RoutingConfiguration = withRouter(_RoutingConfiguration);