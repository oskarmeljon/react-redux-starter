import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { RoutingConfiguration } from 'views';
import store from 'store';

const render = (Component: React.ComponentClass) => ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Component />
        </BrowserRouter>
    </Provider>,
    document.getElementById('root') as HTMLElement,
);

render(RoutingConfiguration);

serviceWorker.unregister();
