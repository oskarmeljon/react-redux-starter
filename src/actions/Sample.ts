import { ThunkDispatch } from 'redux-thunk';
import { TYPES } from 'action-types/Sample'
import { Action } from 'redux';


export const doSomething = () => {
    return async (dispatch: ThunkDispatch<any, any, Action>) => {
        dispatch({ type: TYPES.SAMPLE_ACTION.PENDING });
        try {
            // here goes API call etc.
            dispatch({ type: TYPES.SAMPLE_ACTION.FINISHED });
        } catch (e) {
            // here goes error handling
            dispatch({ type: TYPES.SAMPLE_ACTION.REJECTED });
            throw e;
        }
    }
};